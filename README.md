# DoesItRunWell

[![Build Status](https://travis-ci.org/dafzthomas/doesitrunwell.svg?branch=master)](https://travis-ci.org/dafzthomas/doesitrunwell)

Insight into *when* your application is *able* to do *whatever*, *wherever* it is used.

You do not *know* how it will perform on every device on the planet.

In the future this tool/website will be used to determine whether a browser and the device being use is capable of various features on the web platform. For example, a low end mobile device may have the software to run WebGL animations, but it cannot run at the desired frame rate. This tool would inform the application on that to give the option to intelligently fallback to an implementation that will run on that device, well. :)

Being tested with <a href="https://www.browserstack.com">Browserstack</a> 
<br>
<br>
<a href="https://www.browserstack.com" target="_blank"><img src="https://bstacksupport.zendesk.com/attachments/token/xt5kZvvM9P57v0m5VKSzt0yk4" width="300px" /></a>

## Support on Beerpay
Hey dude! Help me out for a couple of :beers:!

[![Beerpay](https://beerpay.io/dafzthomas/doesitrunwell/badge.svg?style=beer-square)](https://beerpay.io/dafzthomas/doesitrunwell)  [![Beerpay](https://beerpay.io/dafzthomas/doesitrunwell/make-wish.svg?style=flat-square)](https://beerpay.io/dafzthomas/doesitrunwell?focus=wish)
