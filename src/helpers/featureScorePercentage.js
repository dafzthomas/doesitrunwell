function featureScorePercentage(passObj) {
    let featureCount = 0;
    let featureScore = 0;

    for (const property in passObj) {
        if (passObj.hasOwnProperty(property)) {
            if (property !== 'feature_score') {
                featureCount += 1;
                if (passObj[property]) featureScore += 1;
            }
        }
    }

    return Math.floor((featureScore / featureCount) * 100);
}

module.exports = featureScorePercentage;