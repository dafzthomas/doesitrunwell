const featureScorePercentage = require('../helpers/featureScorePercentage.js');

module.exports = function () {
    const elem = document.createElement('canvas');
    const canvas = elem.getContext('2d');

    const passObj = {
        canvas: !!canvas,
        feature_score: 0,
    };

    passObj.feature_score = featureScorePercentage(passObj);

    return passObj;
}