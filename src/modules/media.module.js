const featureScorePercentage = require('../helpers/featureScorePercentage.js');

module.exports = function () {
    const passObj = {
        webm1080p60: false,
        feature_score: 0,
    };
    if ('mediaCapabilities' in navigator) {
        const webm1080p60 = {
            type: 'file',
            video: {
                contentType: 'video/webm',
                width: 1920,
                height: 1080,
                framerate: 60,
            },
        };

        navigator.mediaCapabilities.decodingInfo(webm1080p60).then((result) => {
            passObj.media = (result.supported && result.smooth && result.powerEfficient);
        });

        passObj.feature_score = featureScorePercentage(passObj);

        return passObj;
    }

    return null;
};