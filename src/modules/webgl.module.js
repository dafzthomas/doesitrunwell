const featureScorePercentage = require('../helpers/featureScorePercentage.js');

module.exports = function () {
    const elem = document.createElement('canvas');
    const webgl = elem.getContext('webgl');
    const ANGLE_instanced_arrays = webgl.getExtension('ANGLE_instanced_arrays');
    const EXT_blend_minmax = webgl.getExtension('EXT_blend_minmax');
    const EXT_frag_depth = webgl.getExtension('EXT_frag_depth');
    const EXT_shader_texture_lod = webgl.getExtension('EXT_shader_texture_lod');
    const EXT_texture_filter_anisotropic = webgl.getExtension('EXT_texture_filter_anisotropic');
    const OES_element_index_uint = webgl.getExtension('OES_element_index_uint');
    const OES_standard_derivatives = webgl.getExtension('OES_standard_derivatives');
    const OES_texture_float = webgl.getExtension('OES_texture_float');
    const OES_texture_float_linear = webgl.getExtension('OES_texture_float_linear');
    const OES_texture_half_float = webgl.getExtension('OES_texture_half_float');
    const OES_texture_half_float_linear = webgl.getExtension('OES_texture_half_float_linear');
    const OES_vertex_array_object = webgl.getExtension('OES_vertex_array_object');
    const WEBGL_compressed_texture_s3tc = webgl.getExtension('WEBGL_compressed_texture_s3tc');
    const WEBGL_debug_renderer_info = webgl.getExtension('WEBGL_debug_renderer_info');
    const WEBGL_debug_shaders = webgl.getExtension('WEBGL_debug_shaders');
    const WEBGL_depth_texture = webgl.getExtension('WEBGL_depth_texture');
    const WEBGL_draw_buffers = webgl.getExtension('WEBGL_draw_buffers');
    const WEBGL_lose_context = webgl.getExtension('WEBGL_lose_context');

    const passObj = {
        webgl: !!webgl,
        ANGLE_instanced_arrays: !!ANGLE_instanced_arrays,
        EXT_blend_minmax: !!EXT_blend_minmax,
        EXT_frag_depth: !!EXT_frag_depth,
        EXT_shader_texture_lod: !!EXT_shader_texture_lod,
        EXT_texture_filter_anisotropic: !!EXT_texture_filter_anisotropic,
        OES_element_index_uint: !!OES_element_index_uint,
        OES_standard_derivatives: !!OES_standard_derivatives,
        OES_texture_float: !!OES_texture_float,
        OES_texture_float_linear: !!OES_texture_float_linear,
        OES_texture_half_float: !!OES_texture_half_float,
        OES_texture_half_float_linear: !!OES_texture_half_float_linear,
        OES_vertex_array_object: !!OES_vertex_array_object,
        WEBGL_compressed_texture_s3tc: !!WEBGL_compressed_texture_s3tc,
        WEBGL_debug_renderer_info: !!WEBGL_debug_renderer_info,
        WEBGL_debug_shaders: !!WEBGL_debug_shaders,
        WEBGL_depth_texture: !!WEBGL_depth_texture,
        WEBGL_draw_buffers: !!WEBGL_draw_buffers,
        WEBGL_lose_context: !!WEBGL_lose_context,
        feature_score: 0,
    };

    passObj.feature_score = featureScorePercentage(passObj);

    return passObj;
};